FROM node:latest

RUN npm install -g cnpm --registry=https://registry.npm.taobao.org

VOLUME /tmp

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN cnpm install

COPY . /usr/src/app

EXPOSE 53/udp 53/tcp 10000/tcp

RUN cnpm install -g  babel-cli

CMD [ "npm", "start" ]