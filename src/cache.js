import { LoggerFactory } from './log'

let NodeCache = require("node-cache");
let cache = new NodeCache({
    stdTTL: 100, checkperiod: 120
});
let logger = LoggerFactory.create('cache');

export class CacheHelper {
    static get(key, ttl, provider) {
        return new Promise(function (resolve, reject) {
            let data = cache.get(key);
            if (data) {
                logger.debug(`hit cache ${key}`);
                resolve(data);
                return;
            }

            logger.debug(`miss cache ${key}`);
            let result = provider(key);
            if (!result) {
                reject("provider doest not return any thing.");
                return;
            }
            if (result instanceof Promise) {
                result.then(function success(data) {
                    if (data) {
                        cache.set(key, data, ttl);
                        resolve(data);
                        return;
                    }
                    reject("provider doest not return any thing.");
                }, function fail(err) {
                    reject(err);
                });
                return;
            }
            cache.set(key, result, ttl);
            resolve(result);
            return;
        });
    }
    static destroy(key) {
        cache.del(key);
    }
}
