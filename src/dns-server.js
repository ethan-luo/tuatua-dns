
import { LoggerFactory } from './log'
import { RequestHandler } from './dns/request-handler'

let dns = require('native-dns');
let server = dns.createServer();
let logger = LoggerFactory.create('dns');
let handler = new RequestHandler();

server.on('request', function (request, response) {
    try {
        logger.info(`accept request:${JSON.stringify(request.question) }`);
        handler.handle(request, response)
    } catch (err) {
        logger.error('process request error:' + JSON.stringify(err));
    }
});

server.on('error', function (err, buff, req, res) {
    try {
        logger.error('server error:' + JSON.stringify({
            error: err,
            buff: buff,
            request: req,
            response: res
        }));
    } catch (e) {
        logger.error(`server error when log server error:${e}`);
    }
});

export class DNSServer {
    start() {
        server.serve(53);
        logger.info('DNS proxy server is work.');
    }
}
