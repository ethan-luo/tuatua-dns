﻿import {DNSServer} from './dns-server'
import {DNSManager} from './dns-manager'
import {LoggerFactory} from './log'
import {DatabaseInitializer} from './models/initializer'

let logger = LoggerFactory.create('app');

process.on("beforeExit", function (code) {
    logger.info(`beforeExit with code ${code}`);
})

process.on("exit", function (code) {
    logger.info(`exit with code ${code}`);
})

process.on('uncaughtException', function (err) {
    logger.error(`An uncaught error occurred! ${err}`);
});


try {
    DatabaseInitializer().then(function success() {
        new DNSServer().start();
        new DNSManager().start();
        logger.info(`----application start----`);
    }, function fail(err) {
        logger.fatal(`initialize database fail.`);
    });
} catch (err) {
    logger.fatal(`server start error: ${err}`);
}
