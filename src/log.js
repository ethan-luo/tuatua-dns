


let log4js = require('log4js');

log4js.configure(__dirname+'/../config/log4js-config.json', { reloadSecs: 300 });
export class LoggerFactory {
    static create(category = 'default') {
        return log4js.getLogger(category);
    }
}