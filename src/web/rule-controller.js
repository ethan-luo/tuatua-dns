
import {LoggerFactory} from './../log'
import { CacheHelper } from './../cache'
import {Rule} from './../models/rule'


function list(req, res) {
    Rule.findAll().then(function (rules) {
        res.json({ state: 'success', rules: rules });
    });
}

function create(req, res) {
    let rule = req.body;
    Rule.create(rule).then(function success() {
        CacheHelper.destroy('rule:all');
        res.json({ state: 'success' });
    }, function fail(err) {
        res.json({ state: 'fail', message: err });
    });
}

function edit(req, res) {
    let ruleId = req.params.id;
    let status = req.body.status;
    Rule.findById(ruleId).then(function found(rule) {
        rule.updateAttributes({ status: status })
            .then(function success() {
                CacheHelper.destroy('rule:all');
                res.json({ state: 'success' });
            }, function fail(err) {
                res.json({ state: 'fail', message: err });
            });
    }, function notfound(err) {
        res.json({ state: 'fail', message: 'the rule not exists. ' + err });
    });
}

function del(req, res) {
    let ruleId = req.params.id;
    Rule.destroy({
        where: {
            id: ruleId
        }
    }).then(function success() {
        CacheHelper.destroy('rule:all');
        res.json({ state: 'success' });
    }, function fail(err) {
        res.json({ state: 'fail', message: err });
    });
}

export class RuleController {

    constructor(app) {
        app.get('/rules', list);
        app.post('/rules', create);
        app.post('/rules/:id', edit);
        app.delete('/rules/:id', del);
    }

}