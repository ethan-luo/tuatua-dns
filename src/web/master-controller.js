
import {LoggerFactory} from './../log'
import { CacheHelper } from './../cache'
import {Master} from './../models/master'


function list(req, res) {
    Master.findAll().then(function (masters) {
        res.json({ state: 'success', masters: masters });
    });
}

function create(req, res) {
    let master = req.body;
    Master.create(master).then(function success() {
        CacheHelper.destroy('master:all');
        res.json({ state: 'success' });
    }, function fail(err) {
        res.json({ state: 'fail', message: err });
    });
}

function edit(req, res) {
    let masterId = req.params.id;
    let status = req.body.status;
    Master.findById(masterId).then(function found(master) {
        master.updateAttributes({ status: status })
            .then(function success() {
                CacheHelper.destroy('master:all');
                res.json({ state: 'success' });
            }, function fail(err) {
                res.json({ state: 'fail', message: err });
            });
    }, function notfound(err) {
        res.json({ state: 'fail', message: 'the master not exists. ' + err });
    });
}

function del(req, res) {
    let masterId = req.params.id;
    Master.destroy({
        where: {
            id: masterId
        }
    }).then(function success() {
        CacheHelper.destroy('master:all');
        res.json({ state: 'success' });
    }, function fail(err) {
        res.json({ state: 'fail', message: err });
    });
}

export class MasterController {

    constructor(app) {
        app.get('/masters', list);
        app.post('/masters', create);
        app.post('/masters/:id', edit);
        app.delete('/masters/:id', del);
    }

}