
import {LoggerFactory} from './../log'

function home(req, res) {
    res.render('home.html', function (err, html) {
        res.send(html);
    });
}

export class HomeController {

    constructor(app) {
        app.get('/', home);
    }

}