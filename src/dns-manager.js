import {LoggerFactory} from './log'
import {HomeController} from './web/home-controller'
import {RuleController} from './web/rule-controller'
import {MasterController} from './web/master-controller'

let express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');
let logger = LoggerFactory.create("web");

let app = express();

app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');
app.set('views', `${__dirname}./../views`)

app.use(function (req, res, next) {
    logger.info(`${req.method}:  ${req.url}`);
    next();
});

app.use(bodyParser.urlencoded());

app.use('/static', express.static(`${__dirname}./../static`));

new HomeController(app);
new RuleController(app);
new MasterController(app);

// Compress
app.use(compression());

app.use(function (err, req, res, next) {
    logger.error(err.stack);
    res.status(500).send(JSON.stringify({
        state: 'error',
        message: 'server error',
        stacks: err.stack
    }));
});


export class DNSManager {

    start() {
        app.listen(10000);
        logger.info('manager listening on 10000');
    }

}