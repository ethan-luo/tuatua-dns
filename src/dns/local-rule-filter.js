

import { Rule } from './../models/rule'
import { CacheHelper } from './../cache'
import { LoggerFactory } from './../log'

let dns = require('native-dns');
let logger = LoggerFactory.create("dns");

//cache 1 hour.
function getRules() {
    return CacheHelper.get("rule:all", 3600, function provider() {
        return new Promise(function (resolve, reject) {
            Rule.findAll({
                where: {
                    status: 'enabled'
                },
                order: [
                    ['priority', 'DESC'],
                    ['ID', 'ASC']
                ]
            }).then(function (rules) {
                resolve(rules);
            }, function (err) {
                reject(err);
            });
        });
    });
}


function tryRules(domain) {
    return new Promise(function (resolve, reject) {
        getRules().then(function success(rules) {
            for (let idx = 0; idx < rules.length; idx++) {
                let rule = rules[idx];
                logger.trace(`try rule pattern ${rule.pattern}`);
                try{
                    let pattern=new RegExp(rule.pattern);
                    if (pattern.test(domain)) {
                        resolve(rule);
                        return;
                    }
                }catch(err){
                    logger.error(`invalid rule pattern:${rule.pattern} error: ${err}`);
                }
            }
            reject(`no local rule can match this domain ${domain}.`);
        }, function error(err) {
            logger.error(`match local rules error ${err}`);
            reject(`no local rule can match this domain ${domain}.`);
        });
    });
}

export class LocalRuleFilter {

    handle(request, response) {
        return new Promise(function (resolve, reject) {
            try {
                logger.trace(`try match local rules....`);
                let question = request.question[0];
                tryRules(question.name).then(function success(rule) {
                    try {
                        logger.info(`local rule matched:${JSON.stringify({
                            question: question.name,
                            rule: {
                                pattern:rule.pattern,
                                address:rule.address
                            }
                        }) }`);

                        response.answer.push(dns.A({
                            name: question.name,
                            address: rule.address,
                            ttl: 600,
                        }));
                        response.send();

                        resolve(rule.address);
                        return;
                    } catch (err) {
                        reject(`matched rule but answer error:${JSON.stringify({
                            question: question.name,
                            rule: rule
                        }) }`);
                    }
                }, function error(err) {
                    reject(`local-rule-filter match fail:${JSON.stringify({
                        question: question.name,
                        error: err
                    }) }`);
                });
            } catch (err) {
                reject(`local-rule-filter run error:${err}`);
            }
        });
    }

}
