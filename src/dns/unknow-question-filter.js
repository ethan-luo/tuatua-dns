
import { LoggerFactory } from './../log'

let logger = LoggerFactory.create("dns");

export class UnknowQuestionFilter{

    handle(request, response) {
        return new Promise(function (resolve, reject) {
            try {
                let question = request.question[0];
                logger.info(`unkown question:${question}`);
                response.send();
                reject('unknow question!');
            } catch (err) {
                reject('unknow question!');
                console.error(`unknow-question-filter run error:${err}`);
            }
        });
    }
    
}
