

import { LoggerFactory } from './../log'

let dns = require('native-dns');
let logger = LoggerFactory.create("dns");


export class RemoteMasterRequester {
    constructor(model) {
        this.model = model;
        this.name = model.name;
        this.server = {
            address: model.ip,
            port: model.port,
            type: model.protocol
        }
    }

    toString() {
        return `master(${this.name})`;
    }

    requestRemote(request, response) {
        let requester = this;
        return new Promise(function (resolve, reject) {
            let successFlag = false;
            let remoteRequest = dns.Request({
                question: request.question[0],
                server: requester.server,
                timeout: requester.model.timeout
            });

            remoteRequest.on('message', function (err, remoteResponse) {
                logger.info(`${requester} answer ${JSON.stringify(remoteResponse.answer) }`);
                remoteResponse.answer.forEach(function (answer) {
                    response.answer.push(answer);
                });
                successFlag = true;
            });

            remoteRequest.on('error', function () {
                reject(`${requester} error`);
                response.send();
            });

            remoteRequest.on('timeout', function () {
                reject(`${requester} timeout`);
                response.send();
            });

            remoteRequest.on('end', function () {
                if (successFlag) {
                    response.send();
                    resolve(`${requester} success`);
                } else {
                    reject(`${requester} end but fail`);
                }
            });
            try {
                remoteRequest.send();
            }
            catch (err) {
                reject(`${requester} error: ${err}`);
            }
        });
    }

}