

import { LoggerFactory } from './../log'
import {LocalRuleFilter} from './local-rule-filter'
import {MasterServersFilter} from './master-servers-filter'
import {UnknowQuestionFilter} from './unknow-question-filter'



let filters = [
    new LocalRuleFilter(),
    new MasterServersFilter(),
    new UnknowQuestionFilter()
];
let logger = LoggerFactory.create("dns");


function next(filters, idx, request, response) {
    return new Promise(function (resolve, reject) {
        if (idx >= filters.length) {
            reject('no filters handle this request');
            return;
        }
        let filter = filters[idx];
        filter.handle(request, response).then(function success() {
            resolve();
        }, function fail(err) {
            next(filters, idx + 1, request, response).then(function next_success() {
                resolve();
            }, function next_fail() {
                reject('fail');
            });
        });
    });
}

export class RequestHandler {

    handle(request, response) {
        return next(filters, 0, request, response);
    }

}