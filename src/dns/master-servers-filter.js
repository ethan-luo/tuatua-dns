
import { Master } from './../models/master'
import { CacheHelper } from './../cache'
import { LoggerFactory } from './../log'
import {RemoteMasterRequester} from './remote-master-requester'

let dns = require('native-dns');
let logger = LoggerFactory.create("dns");

//cache 1 hour.
function getMasters() {
    return CacheHelper.get("master:all", 3600, function provider() {
        return new Promise(function (resolve, reject) {
            Master.findAll({
                where: {
                    status: 'enabled'
                },
                order: [
                    ['priority', 'DESC'],
                    ['ID', 'ASC']
                ]
            }).then(function success(masters) {
                resolve(masters);
            }, function error(err) {
                reject(err);
            });
        });
    });
}


function getRequesters() {
    return new Promise(function (resolve, reject) {
        getMasters().then(function success(masters) {
            let requesters = [];
            try {
                for (let idx = 0; idx < masters.length; idx++) {
                    let requester = new RemoteMasterRequester(masters[idx]);
                    requesters.push(requester);
                }
                resolve(requesters);
            } catch (err) {
                logger.error(`get requesters error:${err}`);
                reject(`get requesters error:${err} ${err.stack}`)
            }
        });
    });
}

function next(requesters, idx, request, response) {
    return new Promise(function (resolve, reject) {
        if (idx >= requesters.length) {
            reject('no masters answer');
            return;
        }
        try {
            let requester = requesters[idx];
            let question = request.question[0];
            logger.trace(`ask ${requester} for ${question.name}...`);
            requester.requestRemote(request, response)
                .then(function success(result) {
                    resolve(result);
                }, function fail(err) {
                    logger.trace(`ask ${requester} fail:${err}`);
                    next(requesters, idx + 1, request, response)
                        .then(function success(result) {
                            resolve(result);
                        }, function fail(err) {
                            reject(err);
                        });
                });
        } catch (err) {
            logger.error(`remote requester run error:${err}`);
            next(requesters, idx + 1, request, response)
                .then(function success(result) {
                    resolve(result);
                }, function fail(err) {
                    reject(err);
                });
        }
    });
}


export class MasterServersFilter {

    handle(request, response) {
        return new Promise(function (resolve, reject) {
            try {
                logger.trace(`try resolve from masters....`);
                getRequesters().then(function (requesters) {
                    next(requesters, 0, request, response)
                        .then(function success(result) {
                            resolve(result);
                        }, function error(err) {
                            logger.trace(`all requesters tried but fail.${err}`);
                            reject(err);
                        });
                }, function fail(err) {
                    reject(`master-servers-filter get master fail:${err}`);
                });
            } catch (err) {
                reject(`master-servers-filter run error:${err}`);
            }
        });
    }

}
