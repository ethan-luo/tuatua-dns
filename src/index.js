
import {LoggerFactory} from './log'

let application = null;
let spawn = require('child_process').spawn;
let logger = LoggerFactory.create('app');

function startApp() {
    logger.info('start application');
    application = spawn('node', ['app.js']);
    logger.info('dns application pid is ' + application.pid);
    application.on('close', function (code, signal) {
        logger.error(`info:${JSON.stringify({
            code: code,
            signal: signal
        }) }`);
        application.kill(signal);
        application = startApp();
    });
    application.on('error', function (code, signal) {
        logger.error(`error:${JSON.stringify({
            code: code,
            signal: signal
        }) }`);
        application.kill(signal);
        application = startApp();
    });
    return application;
};

startApp();