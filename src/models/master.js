import {Database} from './database'
let Sequelize = require('sequelize');

export let Master = Database.define('masters', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        allowNull: false,
        type: Sequelize.STRING(64)
    },
    ip: {
        allowNull: false,
        type: Sequelize.STRING(16),
        validate: {
            isIPv4: true,
        }
    },
    port: {
        allowNull: false,
        defaultValue: 53,
        type: Sequelize.INTEGER
    },
    protocol: {
        allowNull: false,
        defaultValue: 'udp',
        type: Sequelize.STRING(8)
    },
    timeout: {
        allowNull: false,
        defaultValue: 2000,
        type: Sequelize.INTEGER,
        comment:'dns查询超时时间，毫秒'
    },
    priority: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.INTEGER
    },
    status: {
        allowNull: false,
        defaultValue: 'enabled',
        type: Sequelize.STRING(32)
    }
});