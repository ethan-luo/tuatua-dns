import { Master } from './master'
import { Database } from './database'
import { LoggerFactory } from './../log'

let Sequelize = require('sequelize');
let logger = LoggerFactory.create("db");

let defaultMasters = [
    {
        name: 'DNSPod',
        ip: '119.29.29.29',
        priority: '100'
    }, {
        name: 'Baidu',
        ip: '180.76.76.76',
        priority: '90'
    }, {
        name: '114DNS',
        ip: '114.114.114.114',
        priority: '80'
    }, {
        name: 'AliDNS',
        ip: '223.6.6.6',
        priority: '70'
    }, {
        name: 'GoogleDNS',
        ip: '8.8.8.8',
        priority: '60'
    }, {
        name: 'OpenDNS',
        ip: '208.67.220.220',
        priority: '50'
    }
];

export function DatabaseInitializer() {

    return new Promise(function (resolve, reject) {
        logger.debug('begin to sync database...');
        Database.sync().then(function success() {
            logger.debug('sync database success...');
            Master.count().then(function (count) {
                logger.debug(`master dns server count is ${count}`);
                if (count > 0) {
                    resolve();
                    return;
                }
                logger.debug(`master is empty,should add default masters`);
                Master.bulkCreate(defaultMasters).then(function (result) {
                    resolve(result);
                }, function (err) {
                    logger.error(`add default master dns servers error:${err}`);
                    reject(err);
                })
            });
        }, function error(err) {
            logger.error(`sync database error:${err}`);
            reject(err);
        });
    });
}
