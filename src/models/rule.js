import {Database} from './database'
let Sequelize = require('sequelize');

export let Rule = Database.define("rules", {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    pattern: {
        allowNull: false,
        type: Sequelize.STRING(256)
    },
    address: {
        allowNull: false,
        type: Sequelize.STRING(16),
        validate: {
            isIPv4: true,
        }
    },
    description: {
        type: Sequelize.STRING(1024)
    },
    priority: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.INTEGER
    },
    status: {
        allowNull: false,
        defaultValue: 'enabled',
        type: Sequelize.STRING(32)
    }
});