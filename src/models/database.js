
import { LoggerFactory } from './../log'

let Sequelize = require('sequelize');
let logger = LoggerFactory.create("model");

export let Database = new Sequelize('tuatua-dns', 'tuatua', '123qwe!@#', {
    dialect: 'sqlite',
    storage: '/tmp/tuatua-dns.sqlite',
    logging: function (something) {
        logger.debug(something);
    }
});