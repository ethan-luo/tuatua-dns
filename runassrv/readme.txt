Run As Service

把任何可执行文件（包括批处理和角本）当作系统服务运行的工具

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
介绍：

  ■ 将任何可执行文件作为系统服务运行。
  ■ 开始主要是为了移植一些linux 的 daemon；也可以用于启动任何应用程序和维护性角本。
  ■ 安静地运行，除非指定了 Interactive 模式，否则不会弹出任何窗口。
  ■ 无需登陆，系统启动后自动运行，无需等待用户登陆完毕。
  ■ 作为附带的功能，支持启动、停止和删除任何系统服务。
  ■ 完整接管系统服务管理器的指令（启动、停止等等）。运行的程序终止后，自动向服务管理器提交状态同时自身退出，不会驻留内存。

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
1.1.0.628 版新增内容：

  ■ 所有 Action （例如：add / remove 等）无需再加前缀。
  ■ 新增 pause / continue 操作。
  ■ 现在开始，一个 runassrv 服务可以并发运行多个 /cmdline 指定的应用。
  ■ runassrv.exe 本身不需要放入系统搜索路径中了。
  ■ 新增 control 操作，向指定的服务发送控制码。
  ■ 新增 listsrv 操作，列出所有服务和驱动，支持很多过滤条件。
  ■ 新增 -logfile 和 -loglevel 选项，日志记录功能。
  ■ 新增 -unstoppable 系统服务管理器将拒绝停止一个以该选项安装的服务。
  ■ 内存收缩策略，典型情况下内存开销少于 300KB。
  ■ 解决了在某些系统上 start / stop 命令无效的问题。

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
1.1.2.803 版新增内容：

  ■ 如果指定了多个 /cmdline 参数，那么服务启动时，用户命令将会按照 /cmdline 的指定顺序逐个执行。
  ■ 每条用户命令都将等待上条 /cmdline 命令就绪后才开始执行，保证有依赖关系的命令可以正常运行。（参见：“关于执行就绪”）
  ■ 为每条命令设置默认的“当前工作目录”参数。例如，有命令：“/cmdline:C:\test\app.exe”，在执行该命令时，其当前工作目录将被设置为“C:\test\”，保证应用程序可以正常访问配置信息和私有数据。

  ■关于执行就绪■ 对于命令行程序（包括批处理和角本），执行就绪意味着标准输入输出设备已初始化完毕，并且已经进入主程序入口（main()）；对于图形界面程序，执行就绪意味着主窗体注册和初始化完毕，程序已经在主循环处等待接受消息。

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
帮助屏幕：

C:\Documents and Settings\Administrator>
#runassrv

===========================================================================
  Run As Service Ver 1.1.2.803 by BaiYang / 2004 - 2006, Freeware
===========================================================================
Run any program as a Service

USAGE: runassrv <Action> <Options>
===========================================================================
Actions:
  add          - Add a Service.
  remove       - Remove a Service.
  start        - Start a Service.
  stop         - Stop a Service.
  pause        - Pause a Service.
  continue     - Continue a paused Service.
  control      - Control a service by send it a op-code
  listsrv      - List all installed services on local system
  listgrp      - List all service load order groups on local system (by start
                 order).
  -h           - this screen
===========================================================================
Options:
  -cmdline     - when the service start, witch program you want to run.
  -name        - the service name.
  -dispname    - service's display name in service manager
  -desc        - service's description in service manager
                   DEFAULT: none
  -type        - service type, can be:
                   FileSystemDriver
                   KernelDriver
                   OwnProcess (DEFAULT)
                   ShareProcess
                   InteractiveProcess
                 NOTE: set this option incorrectly may endanger your system!
  -starttype   - service start type, can be:
                   SystemStart
                   BootStart
                   AutoStart (DEFAULT)
                   DemandStart
                   Disabled
                 NOTE: set this option incorrectly may endanger your system!
  -errctl      - service error control policy, can be:
                   ErrorIgnore
                   ErrorNormal (DEFAULT)
                   ErrorSevere
                   ErrorCritical
                 NOTE: set this option incorrectly may endanger your system!
  -group       - specify service load order group
                   DEFAULT: none
  -depen       - names of services or load ordering groups that the system
                 must start before this service, delimited by '/'
                   DEFAULT: none
  -user        - specifies the name of the account under which the service
                 should run.
                   DEFAULT: LocalSystem
  -pass        - contains the password to the account name
                   DEFAULT: none
  -priority    - priority of the program you want to run, can be:
                   Idle
                   BelowNormal
                   Normal (DEFAULT)
                   AboveNormal
                   High
                   RealTime
  -unstoppable - make service unstoppable by system service control manager.
  -opcode      - for "control", specify the operation code.
  -logfile     - specify the logging target.
                   DEFAULT: don't log to file.
  -loglevel    - specify the lowest logging level, can be:
                   DebugOnly (Most Detail)
                   Info
                   Warning
                   Error (DEFAULT)
                   FatalError
                   Disabled (Close Logging Output)
===========================================================================
Special Options With "listsrv":
  -name        - list service name that match the wildcard pattern
                   DEFAULT: *
  -dispname    - service display name's wildcard pattern.
                   DEFAULT: *
  -pid         - service belong to specified process.
                   DEFAULT: any
  -type        - specify service types to be match, delimite by "/".
                   DEFAULT: any
  -starttype   - specify service starttypes to be match, delimite by "/".
                   DEFAULT: any
  -group       - service belong to the load order group
                   DEFAULT: any
  -state       - service's current state, can be:
                   active
                   inactive
                   DEFAULT: any
===========================================================================
Examples:
  runassrv add /cmdline:"c:\tools\myapp.exe -arg1 -arg2" /name:"mysrv"
  runassrv add /cmdline:myapp.exe /cmdline:yourapp.exe /name:mysrv
  runassrv add /cmdline:notepad /cmdline:vi.bat /type:interactiveprocess /priority:AboveNormal /name:testsrv /logfile:C:\test.log /loglevel:debugonly /dispname:"My Test Service" /desc:"This is a Test Service"

  runassrv start /name:wuauserv
  runassrv stop /name:wuauserv
  runassrv pause /name:iisadmin
  runassrv control /name:wuauserv /opcode:1

  runassrv listsrv /name:w*
  runassrv listsrv /type:KernelDriver/FileSystemDriver
  runassrv listsrv /starttype:SystemStart/BootStart
  runassrv listsrv /group:pnp_tdi
  runassrv listsrv /pid:1234
  runassrv listgrp

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
简单中文帮助：

C:\Documents and Settings\Administrator>
#runassrv

===========================================================================
  Run As Service Ver 1.1.2.803 by BaiYang / 2004 - 2006, Freeware
===========================================================================
将任何可执行程序作为系统服务运行

用法: runassrv <动作> <选项>
===========================================================================
动作:
  add          - 新增一个服务.
  remove       - 删除一个服务.
  start        - 启动一个服务.
  stop         - 停止一个服务.
  pause        - 暂停一个服务
  continue     - 继续一个被暂停的服务.
  control      - 向指定服务发送一个控制码.
  listsrv      - 列出当前系统中符合条件的所有服务及驱动.
  listgrp      - 列出当前系统中所有服务启动组的优先级顺序.
  -h           - 此帮助屏幕.
===========================================================================
选项:
  -cmdline     - 当服务启动时，指定你真正想要运行那个程序. 这个选项可以被多次使
                 用，每个都指定一个要运行的应用。当服务启动时，这些应用将被并发
                 地执行。
  -name        - 指定系统服务名（net start 等系统操作用）.
  -dispname    - 指定用户将要看到的服务显示名称（在服务管理器中看到的名称）.
  -desc        - 指定服务描述（在服务管理器中看到的服务描述信息）
                   默认: 无
  -type        - 指定服务类型，可以是以下几种:
                   FileSystemDriver  : 文件系统及驱动，将在内核载入之前被装入，注
                                       意这个级别的服务几乎不可以使用任何系统API。
                   KernelDriver      : 内核级驱动，内核装入后，立即被装入，这个
                                       级别的服务只能使用受限的API。
                   OwnProcess        : 单独进程服务，标准服务，在Win32子系统ready
                                       后启动，默认。
                   ShareProcess      : 共享进程服务，即数个服务共享同一进程，很多
                                       依赖svchost的服务都使用这种类型。
                   InteractiveProcess: 交互式服务，服务可以与用户交互，除了这个类
                                       型以外，其它所有类型的服务都不可能有用户可
                                       见的界面。
                 注意: 错误的设置这个选项可能使系统变得不稳定，甚至无法启动！如果
                       不知道自己正在做什么，请不要尝试前两项。
  -starttype   - 服务启动类型，包括:
                   SystemStart : 系统级启动，在kernel初始化后启动。
                   BootStart   : 由系统装载器启动（kernel初始化前）。
                   AutoStart   : 自动启动，Win32子系统初始化完毕后启动，默认。
                   DemandStart : 手动。
                   Disabled    : 禁用。
                 注意: 错误的设置这个选项可能使系统变得不稳定，甚至无法启动！如果
                       不知道自己正在做什么，请不要尝试前两项。
  -errctl      - 指定服务出错时的错误处理策略，可以是：
                   ErrorIgnore   : 忽略错误，不挽救服务，系统继续运行
                   ErrorNormal   : 记录到系统日志，并且弹出一个对话框提示用户，默
                                   认。（默认）
                   ErrorSevere   : 记录日志并弹出提示框，如果当前系统已经运行于了
                                   最后一次正确启动的配置，则忽略该错误，否则自动
                                   重启到最后一次正确配置。
                   ErrorCritical : 同上，但是如果重启到最后一次正确配置还是启动本
                                   服务失败就蓝屏。
                 注意: 错误的设置这个选项可能使系统变得不稳定，甚至无法启动！如果
                       不知道自己正在做什么，请不要尝试后两项。
  -group       - 指定服务属于的启动优先级组，优先级组可以通过 listgrp 按顺序列出。
                   默认: 无
  -depen       - 指定本服务所依赖的服务，在这里指定的服务必须在本服务启动之前正确
                 启动。多个服务之间用 “/”分割。这里要指定系统服务名，而不是用户
                 看到的那个名称。
                   默认: 无
  -user        - 指定用户身份，即本服务使用那个用户帐号运行
                   默认: LocalSystem（本地系统，最高权限），交互式服务必须使用这个
                         帐号。
  -pass        - 指定用户密码
                   默认: 无（LocalSystem）
  -priority    - cmdline 参数指定程序的运行优先级，可以是（这个不用解释了吧）:
                   Idle
                   BelowNormal
                   Normal （默认）
                   AboveNormal
                   High
                   RealTime
  -unstoppable - 系统服务管理器将拒绝停止以此选项安装的服务。
  -opcode      - 为 control 命令指定要发送的控制码。控制码 “1”其实相当于命令
                 stop，用于停止服务。除了几个标准的控制码外，大部分控制码所对应的
                 行为由服务自己定义。
  -logfile     - 指定日志文件
                   默认: 不记录日志文件
  -loglevel    - 指定日志记录的详细程度，可以是：
                   DebugOnly （记录所有运行细节）
                   Info
                   Warning
                   Error （默认）
                   FatalError
                   Disabled （关闭对控制台和文件的日志输出）
===========================================================================
使用 listsrv 命令时的特殊选项:
  -name        - 指定要列出的服务名通配符
                   默认: *
  -dispname    - 指定要列出的服务显示名称通配符
                   默认: *
  -pid         - 仅列出运行于指定进程的服务
                   默认: 所有进程
  -type        - 仅列出指定类型的服务, 多个服务类型间使用 "/" 分割
                   默认: 所有类型
  -starttype   - 仅列出指定启动类型的服务, 多个类型间使用 "/" 分割
                   默认: 所有类型
  -group       - 进列出位于指定启动组的服务
                   默认: 任意组
  -state       - 仅列出当前状态满足指定值的服务, 可以是：
                   active   : 服务当前处于活动状态（正在启动、运行等）
                   inactive : 服务当前处于非活动状态（已停止）
                   默认     : 任意
===========================================================================
例子:
  runassrv add /cmdline:"c:\tools\myapp.exe -arg1 -arg2" /name:"mysrv"
  runassrv add /cmdline:myapp.exe /cmdline:yourapp.exe /name:mysrv
  runassrv add /cmdline:notepad /cmdline:vi.bat /type:interactiveprocess /priority:AboveNormal /name:testsrv /logfile:C:\test.log /loglevel:debugonly /dispname:"My Test Service" /desc:"This is a Test Service"

  runassrv start /name:wuauserv
  runassrv stop /name:wuauserv
  runassrv pause /name:iisadmin
  runassrv control /name:wuauserv /opcode:1

  runassrv listsrv /name:w*
  runassrv listsrv /type:KernelDriver/FileSystemDriver
  runassrv listsrv /starttype:SystemStart/BootStart
  runassrv listsrv /group:pnp_tdi
  runassrv listsrv /pid:1234
  runassrv listgrp

===========================================================================
关于命令和参数的分隔符:
  所有参数以 “-” 或 “/” 为前缀；需要赋值得参数以 “:” 或 “=”作为
  赋值运算符，例如：-name="mysrv"; /name:"mysrv"; -name:"mysrv" 等组合都
  是合法的。

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
版权：免费软件；作者不承担任何责任。您可以将它用于任何用途，只要不做坏事。

≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡
联系作者：http://www.cn-dos.net/forum 或 asbai@msn.com

有事尽量论坛或 email 联系。不过要是你喜欢玩星际并且是手速跟我一样低（不超过100）的菜鸟，欢迎加我的 MSN ^_^
